![alt text](https://travis-ci.org/modulon/kernel.svg?branch=master)
![alt text](https://img.shields.io/hexpm/l/plug.svg)

# Modulon v0.1.6 Buttered Potato

Modulon is a 64 bit operating system written in Rust.

# Features

* 64 bit
* VGA module
* Physical page frame allocator
* Panic handling

Version Buttered Potato: Slightly more functional than a potato loaded onto a ROM.

![alt text](https://raw.githubusercontent.com/modulon/kernel/master/screenshot.png)
