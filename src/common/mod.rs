// System reboot function
pub mod power;

// Rust panic_fmt function
pub mod panic;
