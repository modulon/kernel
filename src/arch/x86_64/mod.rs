/// IDT config the x86_64 architecture
pub mod interrupts;

/// Memory management for the x86_64 architecture
pub mod memory;
