// Commonly used colors
use io::display::Color;

pub const WHITE: u8 = Color::new(Color::White, Color::Black);
pub const GREEN: u8 = Color::new(Color::Green, Color::Black);
pub const RED: u8 = Color::new(Color::Red, Color::Black);
pub const LCYAN: u8 = Color::new(Color::LightCyan, Color::Black);
